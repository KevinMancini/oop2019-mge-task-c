using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using Kevin_Mancini;
namespace Kevin_Mancini_Test
{
    [TestClass]
    public class ColorGeneratorTest
    {
        [TestMethod]
        public void StaticColorGeneratorTest()
        {
            const double minlimit = -10.0;
            const double maxlimit = 10.0;
            const double incr = 0.1;
            ColorGenerator cg = new ColorGeneratorImpl(Color.Red);
            for (double i = minlimit; i < maxlimit; i += incr)
            {
                Assert.AreEqual(Color.Red, cg.GetColorFromDouble(i));
            }
            cg = new ColorGeneratorImpl(Color.Green);
            for (double i = minlimit; i < maxlimit; i += incr)
            {
                Assert.AreEqual(Color.Green, cg.GetColorFromDouble(i));
            }
            cg = new ColorGeneratorImpl(Color.Blue);
            for (double i = minlimit; i < maxlimit; i += incr)
            {
                Assert.AreEqual(Color.Blue, cg.GetColorFromDouble(i));
            }
        }
    }
}
