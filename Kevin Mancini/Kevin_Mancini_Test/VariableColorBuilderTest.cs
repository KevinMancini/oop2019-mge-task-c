﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Kevin_Mancini;
namespace Kevin_Mancini_Test
{
    [TestClass]
    public class VariableColorBuilderTest
    {
        [TestMethod]
        public void SetColorsTest()
        {
            int red = 10;
            int green = 150;
            int blue = 0;
            VariableColor vc = (new VariableColorBuilderImpl().setRed(red).setGreen(green).build());
            Assert.IsTrue(vc.GetRed().HasValue);
            Assert.IsTrue(vc.GetGreen().HasValue);
            Assert.IsTrue(!vc.GetBlue().HasValue);
            Assert.IsTrue(vc.GetRed().Equals(red));
            Assert.IsTrue(vc.GetGreen().Equals(green));
            vc = new VariableColorBuilderImpl().setRed(red).setBlue(blue).build();
            Assert.IsTrue(vc.GetRed().HasValue);
            Assert.IsTrue(!vc.GetGreen().HasValue);
            Assert.IsTrue(vc.GetBlue().HasValue);
            Assert.IsTrue(vc.GetRed().Equals(red));
            Assert.IsTrue(vc.GetBlue().Equals(blue));
            vc = new VariableColorBuilderImpl().setBlue(blue).setGreen(green).build();
            Assert.IsTrue(!vc.GetRed().HasValue);
            Assert.IsTrue(vc.GetGreen().HasValue);
            Assert.IsTrue(vc.GetBlue().HasValue);
            Assert.IsTrue(vc.GetGreen().Equals(green));
            Assert.IsTrue(vc.GetBlue().Equals(blue));
            Assert.ThrowsException<InvalidOperationException>(() => new VariableColorBuilderImpl().setBlue(blue).setGreen(green).setRed(red).build());
            vc = new VariableColorBuilderImpl().build();
            Assert.IsFalse(vc.GetRed().HasValue);
            Assert.IsFalse(vc.GetGreen().HasValue);
            Assert.IsFalse(vc.GetBlue().HasValue);
        }
        [TestMethod]
        public void SetMultipleTimes()
        {
            int red = 10;
            int green = 150;
            int blue = 0;
            Assert.ThrowsException<InvalidOperationException>(() => new VariableColorBuilderImpl().setBlue(blue).setBlue(blue));
            Assert.ThrowsException<InvalidOperationException>(() => new VariableColorBuilderImpl().setRed(red).setRed(red));
            Assert.ThrowsException<InvalidOperationException>(() => new VariableColorBuilderImpl().setGreen(green).setGreen(green));
        }
        [TestMethod]
        public void BuildMultipleTimes()
        {
            var builder = new VariableColorBuilderImpl();
            builder.build();
            Assert.ThrowsException<InvalidOperationException>(() => builder.build());
        }
        [TestMethod]
        public void TrySetBadValues()
        {
            int red = -5;
            int green = 1050;
            int blue = -1000;

            /* Set bad blue */
            Assert.ThrowsException<ArgumentException>(() => new VariableColorBuilderImpl().setBlue(blue));
            /* Set bad red */
            Assert.ThrowsException<ArgumentException>(() => new VariableColorBuilderImpl().setRed(red));
            /* Set bad green */
            Assert.ThrowsException<ArgumentException>(() => new VariableColorBuilderImpl().setGreen(green));
        }
    }
}
