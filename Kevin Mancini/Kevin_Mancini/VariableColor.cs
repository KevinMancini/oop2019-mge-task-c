﻿namespace Kevin_Mancini
{
    public interface VariableColor
    {
        int? GetRed();
        int? GetGreen();
        int? GetBlue();
    }
}
