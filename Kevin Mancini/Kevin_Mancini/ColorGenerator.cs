using System.Drawing;
namespace Kevin_Mancini
{
    public interface ColorGenerator {

        Color GetColorFromDouble(double value);

    }
}