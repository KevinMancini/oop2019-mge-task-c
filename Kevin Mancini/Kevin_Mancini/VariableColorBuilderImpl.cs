﻿using System;

namespace Kevin_Mancini
{
    public class VariableColorBuilderImpl : VariableColorBuilder
    {

        private int? red;

        private int? green;

        private int? blue;

        private static readonly int MAXRGBVALUE = 255;

        private static readonly int MINRGBVALUE = 0;

        private Boolean builded = false;

        private Boolean BelongInterval(int value) => (value >= MINRGBVALUE)
                        && (value <= MAXRGBVALUE);

        private int CountColorsSetted()
        {
            int count = 0;
            if (this.red.HasValue) count++;
            if (this.blue.HasValue) count++;
            if (this.green.HasValue) count++;
            return count;
        }

        private void ThrowIllArgExc() => throw new ArgumentException("Error using VariableColorBuilderImpl");

        private void ThrowIllSttExc() => throw new InvalidOperationException("Error using VariableColorBuilderImpl");

        public VariableColorBuilder setRed(int red)
        {
            if (this.red.HasValue
                        || (CountColorsSetted() >= 2))
            {
                ThrowIllSttExc();
            }
            else if (!BelongInterval(red))
            {
                ThrowIllArgExc();
            }
            else
            {
                this.red = red;
                return this;
            }
            return null;
        }

        public VariableColorBuilder setGreen(int green)
        {
            if ((this.green.HasValue
                        || (CountColorsSetted() >= 2)))
            {
                ThrowIllSttExc();
            }
            else if (!BelongInterval(green))
            {
                ThrowIllArgExc();
            }
            else
            {
                this.green = green;
                return this;
            }

            return null;
        }

        public VariableColorBuilder setBlue(int blue)
        {
            if ((this.blue.HasValue
                        || (this.CountColorsSetted() >= 2)))
            {
                ThrowIllSttExc();
            }
            else if (!BelongInterval(blue))
            {
                ThrowIllArgExc();
            }
            else
            {
                this.blue = blue;
                return this;
            }

            return null;
        }

        public VariableColor build()
        {
            if (builded)
            {
                ThrowIllSttExc();
                return null;
            }
            else
            {
                builded = true;
                return new VariableColorImpl(red, green, blue);
            }
        }
    }
}
