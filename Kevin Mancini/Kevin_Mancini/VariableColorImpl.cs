﻿namespace Kevin_Mancini
{
    class VariableColorImpl : VariableColor
    {
        private readonly int? red;
        private readonly int? green;
        private readonly int? blue;
        public int? GetRed() => red;
        public int? GetGreen() => green;
        public int? GetBlue() => blue;
        public VariableColorImpl(int? red,
                                 int? green,
                                 int? blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }
    }
}
