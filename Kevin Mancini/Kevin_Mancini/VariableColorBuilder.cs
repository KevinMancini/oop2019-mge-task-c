﻿using System;

namespace Kevin_Mancini
{
    public interface VariableColorBuilder
    {
        VariableColorBuilder setRed(int red);

        VariableColorBuilder setGreen(int green);

        VariableColorBuilder setBlue(int blue);

        VariableColor build();
    }
}
