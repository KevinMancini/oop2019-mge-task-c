using System.Drawing;
namespace Kevin_Mancini

{
	public class ColorGeneratorImpl : ColorGenerator
	{
		private readonly System.Func<double, Color> colorFunction;
		private const int MAXRGBVALUE = 255;

		public ColorGeneratorImpl(VariableColor varColor, double min, double max)
		{
            double q = -(min == max ? 0 : MAXRGBVALUE / (max - min)) * min;
            System.Func<double, int> linearFunction = (x => (int)((min == max ? 0 : MAXRGBVALUE / (max - min)) * x + q));
			colorFunction = i =>
            {

                i = i > max ? max : i < min ? min : i;
                return Color.FromArgb(varColor.GetRed() ?? linearFunction(i), varColor.GetGreen() ?? linearFunction(i), varColor.GetBlue() ?? linearFunction(i));
            };
		}

        public ColorGeneratorImpl(Color color) => colorFunction = i => color;

        public Color GetColorFromDouble(double value) => colorFunction(value);
    }
}
