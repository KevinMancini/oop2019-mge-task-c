﻿using System.Collections.Generic;

namespace it.unibo.oop.mge.libraries
{
	public class Punctuation : IGenericEnum
	{
		public static readonly Punctuation DOT = new Punctuation("DOT", PunctuationEnum.DOT, ".");
		public static readonly Punctuation COMMA = new Punctuation("COMMA", PunctuationEnum.COMMA, ",");
		public static readonly Punctuation OPEN_PARENTHESES = new Punctuation("OPEN_PARENTHESES", PunctuationEnum.OPEN_PARENTHESES, "(");
		public static readonly Punctuation CLOSE_PARENTHESES = new Punctuation("CLOSE_PARENTHESES", PunctuationEnum.CLOSE_PARENTHESES, ")");
		private static readonly IList<Punctuation> values = new List<Punctuation>();

		static Punctuation()
		{
			GetValues().Add(DOT);
			GetValues().Add(COMMA);
			GetValues().Add(OPEN_PARENTHESES);
			GetValues().Add(CLOSE_PARENTHESES);
		}

		public enum PunctuationEnum
		{
			DOT,
			COMMA,
			OPEN_PARENTHESES,
			CLOSE_PARENTHESES
		}

		public readonly PunctuationEnum punctuationEnumValue;
		private readonly string punctuationName;

		internal Punctuation(string name, PunctuationEnum punctuationEnum, string syntax)
		{
			Syntax = syntax;
			punctuationName = name;
			punctuationEnumValue = punctuationEnum;
		}

		public static IList<Punctuation> GetValues()
		{
			return values;
		}

		public string Syntax { get; }

		public override string ToString() => punctuationName;

		public static Punctuation ValueOf(string name)
		{
			foreach (Punctuation enumInstance in Punctuation.GetValues())
			{
				if (enumInstance.punctuationName == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}
}