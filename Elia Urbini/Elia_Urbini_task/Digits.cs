﻿using System.Collections.Generic;

namespace it.unibo.oop.mge.libraries
{
	public class Digits : IGenericEnum
	{
		public static readonly Digits ZERO = new Digits("ZERO", DigitsEnum.ZERO, "0");
		public static readonly Digits ONE = new Digits("ONE", DigitsEnum.ONE, "1");
		public static readonly Digits TWO = new Digits("TWO", DigitsEnum.TWO, "2");
		public static readonly Digits THREE = new Digits("THREE", DigitsEnum.THREE, "3");
		public static readonly Digits FOUR = new Digits("FOUR", DigitsEnum.FOUR, "4");
		public static readonly Digits FIVE = new Digits("FIVE", DigitsEnum.FIVE, "5");
		public static readonly Digits SIX = new Digits("SIX", DigitsEnum.SIX, "6");
		public static readonly Digits SEVEN = new Digits("SEVEN", DigitsEnum.SEVEN, "7");
		public static readonly Digits EIGHT = new Digits("EIGHT", DigitsEnum.EIGHT, "8");
		public static readonly Digits NINE = new Digits("NINE", DigitsEnum.NINE, "9");
		private static readonly IList<Digits> values = new List<Digits>();

		static Digits()
		{
			GetValues().Add(ZERO);
			GetValues().Add(ONE);
			GetValues().Add(TWO);
			GetValues().Add(THREE);
			GetValues().Add(FOUR);
			GetValues().Add(FIVE);
			GetValues().Add(SIX);
			GetValues().Add(SEVEN);
			GetValues().Add(EIGHT);
			GetValues().Add(NINE);
		}

		public enum DigitsEnum
		{
			ZERO,
			ONE,
			TWO,
			THREE,
			FOUR,
			FIVE,
			SIX,
			SEVEN,
			EIGHT,
			NINE
		}

		public readonly DigitsEnum digitEnumValue;
		private readonly string digitName;

		internal Digits(string name, DigitsEnum digitEnum, string syntax)
		{
			Syntax = syntax;
			digitName = name;
			digitEnumValue = digitEnum;
		}

        public string Syntax { get; }

		public static IList<Digits> GetValues()
		{
			return values;
		}

		public override string ToString() => digitName;

		public static Digits ValueOf(string name)
		{
			foreach (Digits enumInstance in Digits.GetValues())
			{
				if (enumInstance.digitName == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}
}