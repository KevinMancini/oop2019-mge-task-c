﻿using System.Collections.Generic;

namespace it.unibo.oop.mge.libraries
{
	public class Properties : IGenericEnum
	{
		public static readonly Properties MAX = new Properties("MAX", PropertiesEnum.MAX, "Max");
		public static readonly Properties MIN = new Properties("MIN", PropertiesEnum.MIN, "Min");
		private static readonly IList<Properties> values = new List<Properties>();

		static Properties()
		{
			GetValues().Add(MAX);
			GetValues().Add(MIN);
		}

		public enum PropertiesEnum
		{
			MAX,
			MIN
		}

		public readonly PropertiesEnum propertyEnumValue;
		private readonly string propertyName;

		internal Properties(string name, PropertiesEnum propertyEnum, string syntax)
		{
			this.Syntax = syntax;
			propertyName = name;
			propertyEnumValue = propertyEnum;
		}

        public string Syntax { get; }

		public static IList<Properties> GetValues()
		{
			return values;
		}

		public override string ToString() => propertyName;

		public static Properties ValueOf(string name)
		{
			foreach (Properties enumInstance in Properties.GetValues())
			{
				if (enumInstance.propertyName == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}
}