﻿using System.Collections.Generic;

namespace it.unibo.oop.mge.libraries
{
	public class Variable : IGenericEnum
	{
		public static readonly Variable X = new Variable("X", VariableEnum.X, "x");
		public static readonly Variable Y = new Variable("Y", VariableEnum.Y, "y");
		private static readonly IList<Variable> values = new List<Variable>();

		static Variable()
		{
			GetValues().Add(X);
			GetValues().Add(Y);
		}

		public enum VariableEnum
		{
			X,
			Y
		}

		public readonly VariableEnum variableEnumValue;
		private readonly string variableName;

		internal Variable(string name, VariableEnum variableEnum, string syntax)
		{
			this.Syntax = syntax;
			variableName = name;
			variableEnumValue = variableEnum;
		}

		public string Syntax { get; }

		public static IList<Variable> GetValues()
		{
			return values;
		}

		public override string ToString() => variableName;

		public static Variable ValueOf(string name)
		{
			foreach (Variable enumInstance in Variable.GetValues())
			{
				if (enumInstance.variableName == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}
}