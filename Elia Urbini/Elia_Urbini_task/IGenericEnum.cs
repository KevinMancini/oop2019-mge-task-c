﻿namespace it.unibo.oop.mge.libraries
{
	internal interface IGenericEnum
	{
		string Syntax { get; }
	}
}