using it.unibo.oop.mge.libraries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Elia_Urbini_test
{
    [TestClass]
    public class EnumSyntaxTest
    {
        [TestMethod]
        public void VariableSyntaxTest()
        {
            var variables = Variable.GetValues();
            var expected = new List<string> { "x", "y" };
            for (int i = 0; i < variables.Count; i++)
            {
                Assert.AreEqual(variables[i].Syntax, expected[i]);
            }
        }

        [TestMethod]
        public void DigitsSyntaxTest()
        {
            var digits = Digits.GetValues();
            var expected = new List<string> { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
            for (int i = 0; i < digits.Count; i++)
            {
                Assert.AreEqual(digits[i].Syntax, expected[i]);
            }
        }

        [TestMethod]
        public void PropertiesSyntaxTest()
        {
            var properties = Properties.GetValues();
            var expected = new List<string> { "Max", "Min" };
            for (int i = 0; i < properties.Count; i++)
            {
                Assert.AreEqual(properties[i].Syntax, expected[i]);
            }
        }

        [TestMethod]
        public void PunctuationSyntaxTest()
        {
            var punctuation = Punctuation.GetValues();
            var expected = new List<string> { ".", ",", "(", ")" };
            for (int i = 0; i < punctuation.Count; i++)
            {
                Assert.AreEqual(punctuation[i].Syntax, expected[i]);
            }
        }
    }
}
