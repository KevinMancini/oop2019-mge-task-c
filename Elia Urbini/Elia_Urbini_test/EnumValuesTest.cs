using it.unibo.oop.mge.libraries;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Elia_Urbini_test
{
    [TestClass]
    public class EnumValuesTest    {
        [TestMethod]
        public void DigitsValueTest()
        {
            Digits zero = Digits.ValueOf("ZERO");
            Assert.AreEqual(Digits.ZERO, zero);
            Digits one = Digits.ValueOf("ONE");
            Assert.AreEqual(Digits.ONE, one);
            Digits two = Digits.ValueOf("TWO");
            Assert.AreEqual(Digits.TWO, two);
            Digits three = Digits.ValueOf("THREE");
            Assert.AreEqual(Digits.THREE, three);
            Digits four = Digits.ValueOf("FOUR");
            Assert.AreEqual(Digits.FOUR, four);
            Digits five = Digits.ValueOf("FIVE");
            Assert.AreEqual(Digits.FIVE, five);
            Digits six = Digits.ValueOf("SIX");
            Assert.AreEqual(Digits.SIX, six);
            Digits seven = Digits.ValueOf("SEVEN");
            Assert.AreEqual(Digits.SEVEN, seven);
            Digits eight = Digits.ValueOf("EIGHT");
            Assert.AreEqual(Digits.EIGHT, eight);
            Digits nine = Digits.ValueOf("NINE");
            Assert.AreEqual(Digits.NINE, nine);
        }

        [TestMethod]
        public void PropertiesValueTest()
        {
            Properties max = Properties.ValueOf("MAX");
            Assert.AreEqual(Properties.MAX, max);
            Properties min = Properties.ValueOf("MIN");
            Assert.AreEqual(Properties.MIN, min);
        }

        [TestMethod]
        public void PunctuationValueTest()
        {
            Punctuation dot = Punctuation.ValueOf("DOT");
            Assert.AreEqual(Punctuation.DOT, dot);
            Punctuation comma = Punctuation.ValueOf("COMMA");
            Assert.AreEqual(Punctuation.COMMA, comma);
            Punctuation openParentheses = Punctuation.ValueOf("OPEN_PARENTHESES");
            Assert.AreEqual(Punctuation.OPEN_PARENTHESES, openParentheses);
            Punctuation closeParentheses = Punctuation.ValueOf("CLOSE_PARENTHESES");
            Assert.AreEqual(Punctuation.CLOSE_PARENTHESES, closeParentheses);
        }

        [TestMethod]
        public void VariableValueTest()
        {
            Variable x = Variable.ValueOf("X");
            Assert.AreEqual(Variable.X, x);
            Variable y = Variable.ValueOf("Y");
            Assert.AreEqual(Variable.Y, y);
        }
    }
}
