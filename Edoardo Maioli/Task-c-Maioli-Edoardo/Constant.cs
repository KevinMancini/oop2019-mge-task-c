﻿using System;
using System.Collections.Generic;

namespace Task_c_Maioli_Edoardo
{
    public sealed class Constant : IGenericEnum
    {

		public static readonly Constant E = new Constant("E", InnerEnum.E, Math.E);

		public static readonly Constant PI = new Constant("PI", InnerEnum.PI, Math.PI);

		public static readonly Constant PHI = new Constant("PHI", InnerEnum.PHI, 1.6180);

		public static readonly Constant EUMA = new Constant("EUMA", InnerEnum.EUMA, 0.5772);

		public static readonly Constant EMTR = new Constant("EMTR", InnerEnum.EMTR, 0.7025);

		public static readonly Constant PLS = new Constant("PLS", InnerEnum.PLS, 1.3247);

		public static readonly Constant FEIGPR = new Constant("FEIGPR", InnerEnum.FEIGPR, 4.6692);

		public static readonly Constant FEIGSN = new Constant("FEIGSN", InnerEnum.FEIGSN, 2.5029);

		public static readonly Constant PRTW = new Constant("PRTW", InnerEnum.PRTW, 0.6601);

		public static readonly Constant MILLS = new Constant("MILLS", InnerEnum.MILLS, 1.3063);

		public static readonly Constant BRUNTW = new Constant("BRUNTW", InnerEnum.BRUNTW, 1.9021);

		public static readonly Constant BRUMQUAD = new Constant("BRUMQUAD", InnerEnum.BRUMQUAD, 0.8705);

		private static readonly List<Constant> AllConstants = new List<Constant>();

		static Constant()
		{
			AllConstants.Add(E);
			AllConstants.Add(PI);
			AllConstants.Add(PHI);
			AllConstants.Add(EUMA);
			AllConstants.Add(EMTR);
			AllConstants.Add(PLS);
			AllConstants.Add(FEIGPR);
			AllConstants.Add(FEIGSN);
			AllConstants.Add(PRTW);
			AllConstants.Add(MILLS);
			AllConstants.Add(BRUNTW);
			AllConstants.Add(BRUMQUAD);
		}

		public enum InnerEnum
		{
			E,
			PI,
			PHI,
			EUMA,
			EMTR,
			PLS,
			FEIGPR,
			FEIGSN,
			PRTW,
			MILLS,
			BRUNTW,
			BRUMQUAD
		}

		public readonly InnerEnum ConstantEnumValue;
		private readonly string ConstantName;

		private readonly double value;

		internal Constant(string name, InnerEnum innerEnum, double value)
		{
			this.value = value;

			ConstantName = name;
			ConstantEnumValue = innerEnum;
		}

		public double Resolve()
		{
			return value;
		}


		public string Syntax
		{
			get
			{
				return this.ConstantName.ToLower();
			}
		}

		public static Constant ValueOf(string name)
		{
			foreach (Constant enumInstance in Constant.AllConstants)
			{
				if (enumInstance.ConstantName == name)
				{
					return enumInstance;
				}
			}
			throw new ArgumentException(name);
		}
	}

}

