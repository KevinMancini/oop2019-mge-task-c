﻿using System;
using System.Collections.Generic;

namespace TaskCMaioli
{
    public sealed class BracketsUtility
    {
        public static int CountCharacter(String fstring, Func<object, bool> function)
        {
            int cont = 0;
            foreach (char i in fstring)
            {
                if (function.Invoke(i))
                {
                    cont++;
                }
            }
            return cont;

        }

        public static Pair<int, int> CountBrackets(string str)
        {
            return new Pair<int, int>(CountCharacter(str, i => i.Equals('(')), CountCharacter(str, i => i.Equals(')')));
        }


        public static bool CheckBrackets(string currentString)
        {
            Pair<int, int> numBrackets = CountBrackets(currentString);
            return numBrackets.Fst - numBrackets.Snd == 0;
        }

        public static int EndBracket(String str, int offset)
        {
            Pair<int, int> p;
            for (int k = 1; (k < str.Length); k++)
            {
                p = CountBrackets(str.Substring(0, (k + 1)));
                if ((p.Fst == p.Snd))
                {
                    return (k + offset);
                }
            }
            return 0;
        }
    }
}