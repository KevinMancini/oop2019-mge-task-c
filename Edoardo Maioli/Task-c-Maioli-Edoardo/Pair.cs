﻿namespace TaskCMaioli
{
	public class Pair<X, Y>
	{
		public Pair(X fst, Y snd) : base()
		{
			Fst = fst;
			Snd = snd;
		}

        public X Fst { get; }

        public Y Snd { get; }

        public override sealed int GetHashCode()
		{
			const int prime = 31;
			int result = 1;
			result = prime * result + ((Fst == null) ? 0 : Fst.GetHashCode());
			result = prime * result + ((Snd == null) ? 0 : Snd.GetHashCode());
			return result;
		}

		public override sealed bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (!typeof(Pair<X, Y>).IsInstanceOfType(obj))
			{
				return false;
			}
			Pair<X,Y> other = (Pair<X,Y>)obj;
			if (Fst == null)
			{
				if (other.Fst != null)
				{
					return false;
				}
			}
			else if (!Fst.Equals(other.Fst))
			{
				return false;
			}
			if (Snd == null)
			{
				if (other.Snd != null)
				{
					return false;
				}
			}
			else if (!Snd.Equals(other.Snd))
			{
				return false;
			}
			return true;
		}

		public override sealed string ToString()
		{
			return "[" + Fst + ";" + Snd + "]";
		}

	}
}