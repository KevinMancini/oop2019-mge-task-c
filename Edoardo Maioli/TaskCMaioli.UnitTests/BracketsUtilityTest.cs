using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TaskCMaioli.UnitTests
{
    [TestClass]
    public class BracketsUtilityTest
    {
        readonly string str1 = "2*x+1-(pow(4,2)-x)";
        readonly string str2 = "abs(sin(x)) + 5 * exp(pow(-x, 100)) * cos(x)";
        [TestMethod]
        public void CountCharacterTest()
        {

            Assert.IsTrue(BracketsUtility.CountCharacter(str1, i => i.Equals('x')) == 2);
            Assert.IsTrue(BracketsUtility.CountCharacter(str2, i => i.Equals('(') || i.Equals(')')) == 10);
        }

        [TestMethod]
        public void CountBracketsTest()
        {
            string wrongBrackets = "(2+exp(x))+1)";
            Assert.IsTrue(BracketsUtility.CountBrackets(str2).Fst == BracketsUtility.CountBrackets(str2).Snd);
            Assert.IsFalse(BracketsUtility.CountBrackets(wrongBrackets).Fst == BracketsUtility.CountBrackets(wrongBrackets).Snd);
        }

        [TestMethod]
        public void CheckBracketsTest()
        {
            string wrongBrackets = "(2+exp(x))+1)";
            Assert.IsTrue(BracketsUtility.CheckBrackets(str1));
            Assert.IsTrue(BracketsUtility.CheckBrackets(str2));
            Assert.IsFalse(BracketsUtility.CheckBrackets(wrongBrackets));
        }

        [TestMethod]
        public void EndBracketsTest()
        {
            Assert.AreEqual( 17, BracketsUtility.EndBracket(str1.Substring(6), 6));
            Assert.AreEqual(9, BracketsUtility.EndBracket(str2.Substring(7), 7));
        }

    }
}
