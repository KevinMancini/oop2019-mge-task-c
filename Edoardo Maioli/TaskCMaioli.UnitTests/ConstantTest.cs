﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_c_Maioli_Edoardo;

namespace TaskCMaioli.UnitTests
{
    [TestClass]
    public class ConstantTest
    {
        [TestMethod]
        public void CreateConstantTest()
        {
            Constant E = Constant.ValueOf("E");
            Assert.AreEqual(Constant.E, E);

            Constant PHI = Constant.ValueOf("PHI");
            Assert.AreEqual(Constant.PHI, PHI);

            Constant FEIGPR = Constant.ValueOf("FEIGPR");
            Assert.AreEqual(Constant.FEIGPR, FEIGPR);


            Constant BRUMQUAD = Constant.ValueOf("BRUMQUAD");
            Assert.AreEqual(Constant.BRUMQUAD, BRUMQUAD);

        }

        [TestMethod]
        public void ResolveTest()
        {
            Constant E = Constant.ValueOf("E");
            Assert.AreEqual(Math.E, E.Resolve());
            
            Constant PI = Constant.ValueOf("PI");
            Assert.AreEqual(Math.PI, PI.Resolve());

            Constant BRUMQUAD = Constant.ValueOf("BRUMQUAD");
            Assert.AreEqual(0.8705, BRUMQUAD.Resolve());
        }

        [TestMethod]
        public void SyntaxTest()
        {
            Constant E = Constant.ValueOf("E");
            Assert.AreEqual("e",E.Syntax);

            Constant PHI = Constant.ValueOf("PHI");
            Assert.AreEqual("phi", PHI.Syntax);

            Constant FEIGPR = Constant.ValueOf("FEIGPR");
            Assert.AreEqual("feigpr", FEIGPR.Syntax);


            Constant BRUMQUAD = Constant.ValueOf("BRUMQUAD");
            Assert.AreEqual("brumquad", BRUMQUAD.Syntax);
        }

    }
}
