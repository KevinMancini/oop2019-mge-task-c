﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TaskCMaioli.UnitsTests
{
    [TestClass]
    public class PairTests
    {
        readonly Pair<int, int> pairTest1 = new Pair<int, int>(2, 10);
        readonly Pair<string, string> pairTest2 = new Pair<string, string>("Create", "Test");

        [TestMethod]
        public void CreatePairTest()
        {
            Assert.AreEqual(2, pairTest1.Fst);
            Assert.AreEqual(10, pairTest1.Snd);
            Assert.AreEqual("Create", pairTest2.Fst);
            Assert.AreEqual("Test", pairTest2.Snd);

        }

        [TestMethod]
        public void EqualsTest()
        {

            Assert.IsTrue(pairTest1.Equals(new Pair<int, int>(2, 10)));
            Assert.IsFalse(pairTest1.Equals(new Pair<string, string>("2", "10")));

        }
    }
}