using System;

namespace MGEBartolini
{
    interface ILine
    {
        double m { get; }
        double q { get; }
        double zero { get; }
        double SolveFor(double x);
    }
    class Line : ILine
    {
        public Line(IPoint2D a, IPoint2D b)
        {
            if (a.x == b.x)
            {
                this.m = double.PositiveInfinity;
                this.q = 0;
                this.zero = a.x;
            }
            else
            {
                this.m = (a.y - b.y) / (a.x - b.x);
                this.q = a.y - this.m * a.x;
                if (m == 0)
                {
                    this.zero = double.NaN;
                }
                else
                {
                    this.zero = -this.q / this.m;
                }
            }
        }

        public double m { get; private set; }

        public double q { get; private set; }

        public double zero { get; private set; }

        public double SolveFor(double x)
        {
            return this.m * x + this.q;
        }
    }
}