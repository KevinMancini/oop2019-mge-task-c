using System;

namespace MGEBartolini
{
    public interface IPoint3DPerspective
    {
        IPoint2D Render(IPoint3D perspective);
    }

    class Point3DPerspective : IPoint3DPerspective
    {
        private IPoint3D point;

        public Point3DPerspective(IPoint3D point)
        {
            this.point = point;
        }

        public IPoint2D Render(IPoint3D pointOfView)
        {
            double finalX = new Line(new Point2D(this.point.x, this.point.y), new Point2D(pointOfView.x, pointOfView.y)).zero;
            double finalY = new Line(new Point2D(this.point.z, this.point.y), new Point2D(pointOfView.z, pointOfView.y)).zero;
            return new Point2D(finalX, finalY);
        }
    }
}
