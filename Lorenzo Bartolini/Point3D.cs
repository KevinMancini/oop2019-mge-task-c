using System;

namespace MGEBartolini
{
    public interface IPoint3D
    {
        double x { get; }
        double y { get; }
        double z { get; }

        IPoint3D Rotated(double angleXY, double angleYZ);

        IPoint3D Transformed(Func<double, double> transformation);

        IPoint3D Translated(double x, double y, double z);

        IPoint3D Translated(IPoint3D vector);
    }

    class Point3D : IPoint3D
    {
        public Point3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double x { get; private set; }

        public double y { get; private set; }

        public double z { get; private set; }

        public override bool Equals(object obj)
        {
            return obj is Point3D d &&
                   x == d.x &&
                   y == d.y &&
                   z == d.z;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y, z);
        }

        public IPoint3D Rotated(double angleXY, double angleYZ)
        {
            return this.RotatedXY(angleXY).RotatedYZ(angleYZ);
        }

        public IPoint3D Transformed(Func<double, double> transformation)
        {
            return new Point3D(transformation.Invoke(this.x), transformation.Invoke(this.y), transformation.Invoke(this.z));
        }

        public IPoint3D Translated(double x, double y, double z)
        {
            return new Point3D(this.x + x, this.y + y, this.z + z);
        }

        public IPoint3D Translated(IPoint3D vector)
        {
            return new Point3D(this.x + vector.x, this.y + vector.y, this.z + vector.z);
        }

        private Point3D RotatedXY(double angle)
        {
            return new Point3D(this.x * Math.Cos(angle) - this.y * Math.Sin(angle),
                this.x * Math.Sin(angle) + this.y * Math.Cos(angle), this.z);
        }

        private Point3D RotatedYZ(double angle)
        {
            return new Point3D(this.x, this.y * Math.Cos(angle) - this.z * Math.Sin(angle),
                this.y * Math.Sin(angle) + this.z * Math.Cos(angle));
        }
    }
}
