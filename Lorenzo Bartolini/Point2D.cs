using System;

namespace MGEBartolini
{
    public interface IPoint2D
    {
        double x { get; }
        double y { get; }

        IPoint2D Transformed(Func<double, double> transformation);
    }

    class Point2D : IPoint2D
    {
        public Point2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double x { get; private set; }

        public double y { get; private set; }

        public override bool Equals(object obj)
        {
            return obj is Point2D d &&
                   x == d.x &&
                   y == d.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }

        public IPoint2D Transformed(Func<double, double> transformation)
        {
            return new Point2D(transformation.Invoke(this.x), transformation.Invoke(this.y));
        }
    }

}
