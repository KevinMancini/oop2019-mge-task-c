using System;
using Xunit;

namespace MGEBartolini
{
    public class RenderTests
    {
        IPoint3D pov = new Point3D(0, -200, 0);
        [Theory]
        [InlineData(0, 0, 0, 0, 0)]
        [InlineData(100, 0, 0, 100, 0)]
        public void RenderTest(double sx, double sy, double sz, double rx, double ry)
        {
            IPoint3D source = new Point3D(sx, sy, sz);
            IPoint2D renderedPoint = new Point3DPerspective(source).Render(pov);
            Assert.Equal(renderedPoint, new Point2D(rx, ry));
        }
    }
}
